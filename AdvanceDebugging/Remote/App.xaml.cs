﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace Remote
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        /// <summary>
        /// 1. Install Remote Tools:
        ///     https://docs.microsoft.com/en-us/visualstudio/debugger/remote-debugging
        ///     
        /// 2. Run Remote Tools.
        /// 
        /// 4. Remote debugging on application startup (uncomment WaitForDebugger(args))
        /// 
        /// </summary>
        /// <param name="args"></param>
        [STAThread]
        public static void Main(string[] args)
        {
            // WaitForDebugger(args);

            var application = new App();
            application.InitializeComponent();
            application.Run();
        }

        /// <summary>
        /// Wait for attach remoted debugger if any argument is equals to "-debug".
        /// </summary>
        /// <param name="args">Program arguments.</param>
        private static void WaitForDebugger(string[] args)
        {
            if (args != null && args.Any(x => x.Contains("-debug")))
            {
                while (!Debugger.IsAttached)
                {
                    Thread.Sleep(100);
                }
            }
        }
    }
}
