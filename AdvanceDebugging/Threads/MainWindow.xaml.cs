﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Threads
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 1. Application creates 4 threads. 2 threads displays power of elements from 0 to 9,
        ///     another 2 threads displays sum of elements from 0 to 9.
        ///     
        /// 2. Threads window:
        ///     a. Freez - follow only one thread.
        ///     b. Call stacks for each thread
        ///     c. Filtering threads
        ///     d. Flag thread - show only flagged threads
        ///     
        /// 3. Parallel stack
        /// 4. Conditional breakpoints - debug selected threads
        ///     (ThreadName == "Sum thread 0")
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_Click(object sender, RoutedEventArgs e)
        {
            for(int i = 0; i < 2; i++)
            {
                Thread thread = new Thread(SumPrinter);
                thread.Name = "Sum thread " + i;
                thread.Start();

                thread = new Thread(PowPrinter);
                thread.Name = "Pow thread " + i;
                thread.Start();
            }
        }

        private void PowPrinter()
        {
            for (int i = 0; i < 10; i++)
            {
                Thread.Sleep(10);
                Thread currentThread = Thread.CurrentThread;
                int pow = i * i;
                Console.WriteLine(currentThread.Name + " pow is: " + pow);
            }
        }

        private void SumPrinter()
        {
            for(int i = 0; i < 10; i++)
            {
                Thread.Sleep(10);
                Thread currentThread = Thread.CurrentThread;
                int sum = i + i;
                Console.WriteLine(currentThread.Name + " sum is: " + sum);
            }
        }
    }
}
