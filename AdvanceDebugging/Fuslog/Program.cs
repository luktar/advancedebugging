﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fuslog
{
    class Program
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// 1. Show references - assemlby NLog exists
        /// 2. Activate FUSLOG by registry (Fuslog.reg)
        /// 
        ///     HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Fusion
        ///     DWORD ForceLog set value to 1
        ///     DWORD LogFailures set value to 1
        ///     DWORD LogResourceBinds set value to 1
        ///     DWORD EnableLog set value to 1
        ///     String LogPath set value to folder for logs(e.g. C:\Fuslog\)
        ///     
        /// 3. Build application, run with all references
        /// 4. Delete NLog.dll in ./Bin/Debug directory.
        /// 5. Check C:\Fuslog\Fuslog directory for missing references.
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            logger.Info("Hello world!");
            logger.Info("Press any key to close application...");
            Console.ReadKey();
        }
    }
}
