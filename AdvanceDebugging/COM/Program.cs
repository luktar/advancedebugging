﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Excel;
using System.Runtime.InteropServices;

namespace COM
{
    class Program
    {
        /// <summary>
        /// 1. Add reference to COM object - "Microfot Excel 16.0 Object Library".
        /// 2. Show errors on preview.
        /// 2. Go to tools -> options -> Debugging -> General
        ///     Check "Use Managed Compatibility Mode".
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            try
            {
                Application xlApp = new Application();

                Workbook xlWorkBook;
                Worksheet xlWorkSheet;
                object misValue = System.Reflection.Missing.Value;

                xlWorkBook = xlApp.Workbooks.Add(misValue);
                xlWorkSheet = (Worksheet)xlWorkBook.Worksheets.get_Item(1);

                xlWorkSheet.Cells[1, 1] = "ID";
                xlWorkSheet.Cells[1, 2] = "Name";
                xlWorkSheet.Cells[2, 1] = "1";
                xlWorkSheet.Cells[2, 2] = "One";
                xlWorkSheet.Cells[3, 1] = "2";
                xlWorkSheet.Cells[3, 2] = "Two";



                xlWorkBook.SaveAs(
                    "c:\\temp\\Excel\\csharp-Excel.xls", XlFileFormat.xlWorkbookNormal, misValue, 
                    misValue, misValue, misValue, XlSaveAsAccessMode.xlExclusive, misValue, 
                    misValue, misValue, misValue, misValue);

                xlWorkBook.Close(true, misValue, misValue);
                xlApp.Quit();

                Marshal.ReleaseComObject(xlWorkSheet);
                Marshal.ReleaseComObject(xlWorkBook);
                Marshal.ReleaseComObject(xlApp);
            } finally
            {

            }
        }
    }
}
