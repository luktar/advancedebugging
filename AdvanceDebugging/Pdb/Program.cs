﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pdb
{
    class Program
    {
        /// <summary>
        /// 1. Compile program and run it with pdb file.
        /// 2. Delete pdb and rum program.
        ///     Exception has less details.
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            try
            {
                TopMethod("osiem");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();

        }

        static void TopMethod(string value)
        {
            SubMethod(value);
        }

        static void SubMethod(String value)
        {
            double a = double.Parse(value);
        }
    }
}
