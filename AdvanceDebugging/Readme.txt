Proposals:

1. Fuslog - basics
2. Create DMP files with registry.

Links:
1. Start fuslog by registry: 
	https://stackoverflow.com/questions/255669/how-to-enable-assembly-bind-failure-logging-fusion-in-net
2. Remote Tools For Windows:
	https://docs.microsoft.com/en-us/visualstudio/debugger/remote-debugging
3. Dmp with registry:
	https://msdn.microsoft.com/en-us/library/windows/desktop/bb787181(v=vs.85).aspx
4. What's COM object
	https://www.codeproject.com/Articles/633/Introduction-to-COM-What-It-Is-and-How-to-Use-It
5. Debug COM:
	https://codedocu.com/Details?d=1793&a=9&f=256&l=0&v=d
