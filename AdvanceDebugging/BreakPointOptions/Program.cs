﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BreakPointOptions
{
    /// <summary>
    /// Breakpoint:
    /// 
    /// 1. Conditions
    ///     a. HitCount (50)
    ///     b. Expression (i % 5 == 0)
    ///     c. Filter (ThreadName == "Main Thread")
    /// 
    /// 2. Actions 
    ///     a. Usage of parenthesis {}
    ///     b. "List every time:" {GetString(i)}
    /// 
    /// 3. Label
    /// 
    /// Immediate:
    /// 
    /// 1. Variable preview
    /// 2. Method result preview ( GetString(8) )
    /// 3. Change variable
    /// 4. Comparition of two variables
    /// 
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            int counter = 0;

            for(int i = 0; i < 100; i++)
            {
                if(i % 10 == 0)
                {
                    Console.WriteLine(GetString(i));
                    counter++;
                }
            }

            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
        }

        private static string GetString(double number)
        {
            return "Number divisible by 10: " + number;
        }
    }
}
