﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Dump
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// 1. Just crash
        ///     a. Create Reg entry
        ///     b. Crash application
        ///     d. Read DMP file
        ///     c. Load symbols - Modules Window
        ///     
        /// 2. UI crash
        ///     a. Create dmp
        ///     b. Localize exception by Parallel Stack
        ///     
        /// 3. Deadlock
        ///     a. Create dmp from task manager
        ///     b. Open and debug dmp
        ///     c. Use Parallel Stack to localize deadlock
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
        }

        #region JustCrash

        private void buttonJustCrash_Click(object sender, RoutedEventArgs e)
        {
            double osiem = FantasticConverter("osiem");
        }

        private double FantasticConverter(string value)
        {
            return double.Parse(value);
        }

        #endregion

        #region ThreadCrash

        private void buttonThreadCrash_Click(object sender, RoutedEventArgs e)
        {
            ThreadStart threadStart = new ThreadStart(UpdateLabel);
            Thread thread = new Thread(threadStart);
            thread.Start();

            for(int i = 0; i < 10; i++)
            {
                Thread.Sleep(100);
            }
        }

        private void UpdateLabel()
        {
            labelMessage.Content = "New content";
        }

        #endregion

        #region DeadlockCrash

        static object object1 = new object();
        static object object2 = new object();

        private void buttonDeadlock_Click(object sender, RoutedEventArgs e)
        {
            Thread deadlockThread = new Thread((ThreadStart)DeadlockFunction);
            deadlockThread.Start();

            lock (object2)
            {
                Thread.Sleep(1000);
                lock (object1)
                {
                }
            }
        }

        public static void DeadlockFunction()
        {
            lock (object1)
            {
                Thread.Sleep(1000);
                lock (object2)
                {
                }
            }
        }

        #endregion

    }
}
